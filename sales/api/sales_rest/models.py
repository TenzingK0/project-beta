from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    model_name = models.CharField(max_length=200)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=50, unique=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.pk})


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12, unique=True)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.pk})


class SaleRecord(models.Model):
    sale_price = models.PositiveSmallIntegerField()

    employee_number = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer_name = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="Sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_sale_record", kwargs={"pk": self.pk})
