from django.urls import path
from .views import api_list_sales_person, api_sales_person, api_list_customers, api_customer, api_list_sale_records, api_sale_record

urlpatterns = [
    path("sales_person/", api_list_sales_person, name="api_list_sales_person"),
    path("sales_person/<int:pk>/", api_sales_person, name="api_sales_person"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("sale_records/", api_list_sale_records, name="api_list_sale_records"),
    path("sale_records/<int:pk>/", api_sale_record, name="api_sale_record"),
]
