from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import AutomobileVO, SalesPersonEncoder, CustomerEncoder, SaleRecordEncoder
from .models import AutomobileVO, SalesPerson, Customer, SaleRecord


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": sale_records},
            encoder=SaleRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            customer_name = Customer.objects.get(id=content["customer_name"])
            content["customer_name"] = customer_name
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            employee_number = SalesPerson.objects.get(id=content["employee_number"])
            content["employee_number"] = employee_number
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales person id"},
                status=400,
            )
        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder=SaleRecordEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_sale_record(request, pk):
    if request.method == "GET":
        sale_record = SaleRecord.objects.get(id=pk)
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordEncoder,
            safe=False,
        )
    else:
        count, _ = SaleRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
