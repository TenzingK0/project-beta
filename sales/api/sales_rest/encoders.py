from common.json import ModelEncoder

from .models import SalesPerson, Customer, SaleRecord, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "model_name",
        "vin",
        "import_href",
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "sale_price",
        "employee_number",
        "customer_name",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "employee_number": SalesPersonEncoder(),
        "customer_name": CustomerEncoder(),
    }
    def get_extra_data(self, o):
        return {
            "automobile": {
                "model_name": o.automobile.model_name,
                "vin": o.automobile.vin,
            }
        }
