import { useEffect, useState } from "react";

function SaleRecordList(props) {
  const [saleRecords, setSaleRecords] = useState([]);

  const fetchSaleRecordsData = async () => {
    const saleRecordsUrl = 'http://localhost:8090/api/sale_records/';

    const response = await fetch(saleRecordsUrl);

    if (response.ok) {
      const data = await response.json();
      setSaleRecords(data.sale_records);
    }
  }

  useEffect(() => {
    fetchSaleRecordsData();
  }, []);

  return (
    <div className="container">
      <h2>Sale Record</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales person</th>
            <th>Employee number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale price</th>
          </tr>
        </thead>
        <tbody>
          {saleRecords.map(saleRecord => {
            return (
              <tr key={saleRecord.id}>
                <td>{ saleRecord.employee_number.name }</td>
                <td>{ saleRecord.employee_number.employee_number }</td>
                <td>{ saleRecord.customer_name.name }</td>
                <td>{ saleRecord.automobile.vin }</td>
                <td>${ saleRecord.sale_price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SaleRecordList;
