import { useState, useEffect } from "react";

function AppointmentForm() {
    const [name, setName] = useState('');
    const handlenameChange = event => setName(event.target.value);

    const [vin, setVin] = useState('');
    const handleVinChange = event => setVin(event.target.value);

    const [technician, setTechnician] =  useState ('');
    const handleTechnicianChange = event => setTechnician(event.target.value);

    const [technicians, setTechnicans] = useState([]);

    const [apptDate, setApptDate] = useState('');
    const handleApptDateChange = event => setApptDate(event.target.value);


    const [reason, setReason] = useState ('');
    const handlereasonChange = event => setReason(event.target.value);


    const handleSubmit = async event => {
        event.preventDefault();

        const data = {};
        data.owner_name = name;
        data.vin = vin;
        data.assigned_tech = technician;
        data.service_reason = reason;
        data.service_time = apptDate;

        const technicianURL = 'http://localhost:8080/api/service/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
            setName('');
            setVin('');
            setTechnician('');
            setApptDate('');
            setReason('');
        }
    }
    const fetchTechnician = async() => {
        const url = 'http://localhost:8080/api/service/employee/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicans(data.technician);
        }
    }
    useEffect(() =>{
        fetchTechnician();
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={handlenameChange} value={name} placeholder="Owner_Name" required type="text" name="Owner_Name" id="Owner_Name" className="form-control" />
                        <label htmlFor="Owner_Name">Owner Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="Vin" id="Vin" className="form-control" />
                        <label htmlFor="Vin">Enter Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleApptDateChange} value={apptDate} placeholder="service_time" required type="datetime-local" name="service_time" id="service_time" className="form-control" />
                        <label htmlFor="vin">Date and Time</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlereasonChange} value={reason} placeholder="Service_Reason" required type="text" name="Date/Time" id="Service_Reason" className="form-control" />
                        <label htmlFor="vin">Reason for Service</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleTechnicianChange} value={technician} required id="Technician_Name" name="Technician_Name" className="form-select">
                            <option>Assign a Technician</option>
                            {technicians.map(technician => {
                                return(
                                    <option key={technician.id} value={technician.id}>
                                        {technician.tech_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )
}
export default AppointmentForm
