import { useEffect, useState } from "react";

function CustomerList(props) {
  const [customers, setCustomers] = useState([]);

  const fetchCustomerData = async () => {
    const customersUrl = 'http://localhost:8090/api/customers/';

    const response = await fetch(customersUrl);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  useEffect(() => {
    fetchCustomerData();
  }, []);

  return (
    <div className="container">
      <h2>Customers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone number</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer => {
            return (
              <tr key={customer.href}>
                <td>{ customer.name }</td>
                <td>{ customer.address }</td>
                <td>{ customer.phone_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default CustomerList;
