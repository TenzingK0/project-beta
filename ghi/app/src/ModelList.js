import { useEffect, useState } from "react";


function ModelList(props) {
  const [models, setModels] = useState([]);

  const fetchModelData = async () => {
    const modelUrl = 'http://localhost:8100/api/models/';

    const response = await fetch(modelUrl);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    fetchModelData();
  }, []);

  return (
    <div className="container">
      <h2>Vechile models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={model.id}>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img src={ model.picture_url } width="240" height="150"/> </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ModelList;
