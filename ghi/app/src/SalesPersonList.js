import { useEffect, useState } from "react";

function SalesPersonList(props) {
  const [salesPeople, setSalesPeople] = useState([]);

  const fetchSalesPeopleData = async () => {
    const salesPeopleUrl = 'http://localhost:8090/api/sales_person/';

    const response = await fetch(salesPeopleUrl);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_person);
    }
  }

  useEffect(() => {
    fetchSalesPeopleData();
  }, []);
  
  return (
    <div className="container">
      <h2>Sales People</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>employee number</th>
          </tr>
        </thead>
        <tbody>
          {salesPeople.map(person => {
            return (
              <tr key={person.href}>
                <td>{ person.name }</td>
                <td>{ person.employee_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesPersonList;
