import React, {useEffect, useState } from 'react';

function AppointmentList() {
    const [query, setQuery] = useState("")
    const [appointments, setAppointments] = useState([]);
    const [filteredApps, setFilteredApps] = useState([]);

    const fetchAppointments = async () => {
        const url = 'http://localhost:8080/api/service/appointments/';
        const response = await fetch(url);

        const data = await response.json();

        if (response.ok) {
            setAppointments(data.appointments);
            setFilteredApps(data.appointments);
        }
    };

    useEffect(() => {
        fetchAppointments();
    }, []);

    useEffect(() => {
        const isValidQuery = query === "" || query === undefined || query === null
        const filteredApps = isValidQuery ? appointments :
            appointments.filter((app) => {
                return app.vin.includes(query)
            })
        setFilteredApps(filteredApps);
    }, [query, appointments])

    function getTableIcon(check) {
        return check ?
            <i className="fa-solid fa-check text-success"></i> :
            <i className="fa-regular fa-circle-xmark text-danger"></i>
    }

    async function deleteAppointment(id) {
        const url = `http://localhost:8080/api/service/appointments/${id}`;
        const response = await fetch(url, { method: 'DELETE' });

        if (response.ok) {
            fetchAppointments();
        }
    }
//will be identical to the delete,
//use a put, to update the iscomplete status to true
//when the user clicks finished
//	"vin": "12345678901234567",
	// "owner_name": "Finnly",
	// "service_time": "2023-03-21T00:00:00+00:00",
	// "assigned_tech": 11,
	// "iscomplete": true,
	// "id": 18
async function finishAppointment(id) {
    const finishUrl = `http://localhost:8080/api/service/appointments/${id}/`;
    const response = await fetch(finishUrl);
    const appointmentData =await response.json();
    const data = {};
    data.vin = appointmentData.vin;
    data.owner_name = appointmentData.owner_name;
    data.service_time = appointmentData.service_time;
    data.assigned_tech = appointmentData.assigned_tech.id;
    data.id = appointmentData.id;
    data.iscomplete = true;
    const fetchconfig = {
        method: 'put',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const finishResponse = await fetch(finishUrl,fetchconfig);
    if (finishResponse.ok) {
        const copyAppointments = [...appointments];
        const filteredAppointments= copyAppointments.filter(s => s.id !== appointments.id);
        setAppointments(filteredAppointments);
    }
}

    function getButtons(id) {
        return (
            <div>
                <button type="button" onClick={() => deleteAppointment(id)} className="btn btn-danger btn-sm rounded-0 mx-2">Cancel</button>
                <button type="button" onClick={() => finishAppointment(id)} className="btn btn-success btn-sm rounded-0 mx-2">Finished</button>
            </div>
        )
    }

    return (
        <div>
        <input placeholder="Enter a Vin" onChange={event => setQuery(event.target.value)} />
        <div>
            <h1>Appointment History</h1>
        </div>
            <table className="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Owner Name</th>
                        <th>Assigned Tech</th>
                        <th>VIN Number </th>
                        <th>Service Time</th>
                        <th>Service Reason</th>
                        <th>Is Complete</th>
                        <th>VIP</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {filteredApps.map((appointment) => (
                    <tr key={appointment.id}>
                        <td>{appointment.owner_name}</td>
                        <td>{appointment.assigned_tech.tech_name}</td>
                        <td>{appointment.vin}</td>
                        <td>{appointment.service_time}</td>
                        <td>{appointment.service_reason}</td>
                        <td>{getTableIcon(appointment.iscomplete)}</td>
                        <td>{getTableIcon(appointment.vip)}</td>
                        <td className="text-center">{getButtons(appointment.id)}</td>
                    </tr>
                    )
                )}
                </tbody>
            </table>
        </div>
    );
}
export default AppointmentList;
