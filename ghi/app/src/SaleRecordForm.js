import React, {useEffect, useState} from "react";
import { useNavigate } from 'react-router-dom';


function SaleRecordForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salesPeople, setSalesPeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salePrice, setSalePrice] = useState('');
  const [automobile, setAutomobile] = useState('');
  const [salesPerson, setSalesPerson] = useState('');
  const [customer, setCustomer] = useState('');
  const navigate = useNavigate();

  const handleSalePriceChange = (event) => {
    const value = event.target.value;
    setSalePrice(value);
  }

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  }

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.sale_price = salePrice;
    data.employee_number = salesPerson;
    data.customer_name = customer;
    data.automobile = automobile;

    const saleRecordUrl = 'http://localhost:8090/api/sale_records/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json',
      },
    };

    const response = await fetch(saleRecordUrl, fetchConfig);
    if (response.ok) {
      setAutomobile('');
      setSalesPerson('');
      setCustomer('');
      setSalePrice('');
      navigate("/sale_records");
    }
  }

  const fetchSalesPeopleData = async () => {
    const url = 'http://localhost:8090/api/sales_person/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_person);
    }
  }
  useEffect(() => {
    fetchSalesPeopleData();
  }, []);

  const fetchCustomerData = async () => {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }
  useEffect(() => {
    fetchCustomerData();
  }, []);

  const fetchAutomobileData = async () => {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const saleRecordUrl = 'http://localhost:8090/api/sale_records/';

    const automobileResponse = await fetch(automobileUrl);
    const saleRecordResponse = await fetch(saleRecordUrl);
    if (automobileResponse.ok && saleRecordResponse.ok) {
      const automobileData = await automobileResponse.json();
      const saleRecordData = await saleRecordResponse.json();
      const newSaleRecordData = [];
      automobileData.autos.map(automobile => {
        let autoSold = false;
        saleRecordData.sale_records.map(saleRecord => {
          if (automobile.vin === saleRecord.automobile.vin) {
            return (autoSold = true);
          }
        });
        if (autoSold === false) {
            newSaleRecordData.push(automobile)
        }
      });
      setAutomobiles(newSaleRecordData);
    }
  }

  useEffect(() => {
    fetchAutomobileData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
          <div className="mb-3">
              <select onChange={handleAutomobileChange} value={automobile} name="autos" required id="autos" className="form-select">
                <option value="">Choose an automobile</option>
                {automobiles.map(automobile => {
                  return (
                    <option key={automobile.href} value={automobile.href}>
                      {automobile.year} {automobile.color} {automobile.model.manufacturer.name} {automobile.model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalesPersonChange} value={salesPerson} name="sales_person" required id="sales_person" className="form-select">
                <option value="">Choose a sales person</option>
                {salesPeople.map(salesPerson => {
                  return (
                    <option key={salesPerson.id} value={salesPerson.id}>
                      {salesPerson.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleCustomerChange} value={customer} name="customer" required id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleSalePriceChange} value={salePrice} placeholder="Sale price" required type="number" name="sale_price" id="sale_price" className="form-control"/>
              <label htmlFor="sale_price">Sale price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleRecordForm;
