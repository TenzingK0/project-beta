import { useEffect, useState } from "react";

function AutomobileList(props) {
  const [automobiles, setAutomobiles] = useState([]);

  const fetchAutomobileData = async () => {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(automobileUrl);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }

  useEffect(() => {
    fetchAutomobileData();
  }, []);

  return (
    <div className="container">
      <h2>Vehicle models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.id}>
                <td>{ automobile.vin }</td>
                <td>{ automobile.color }</td>
                <td>{ automobile.year }</td>
                <td>{ automobile.model.name }</td>
                <td>{ automobile.model.manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
