import React, { useEffect, useState } from "react";


function SalesPersonHistoryList(props) {
  const [salesPeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState('');
  const [saleRecords, setSaleRecords] = useState([]);

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }

  const fetchSalesPeopleData = async () => {
    const salesPeopleUrl = 'http://localhost:8090/api/sales_person/';

    const response = await fetch(salesPeopleUrl);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_person);
    }
  }

  useEffect(() => {
    fetchSalesPeopleData();
  }, []);

  const fetchSaleRecordData = async () => {
    const saleRecordUrl = 'http://localhost:8090/api/sale_records/';

    const response = await fetch(saleRecordUrl);

    if (response.ok) {
      const data = await response.json();
      const saleRecordData = [];
      data.sale_records.map(sale_record => {
        if (salesPerson === sale_record.employee_number.employee_number) {
          saleRecordData.push(sale_record);
        }
      });
      setSaleRecords(saleRecordData);
    }
  }

  useEffect(() => {
    fetchSaleRecordData();
  }, [salesPerson]);


  return (
    <div className="container">
      <h2>Sales person history</h2>
      <div className="mb-3">
        <select onChange={handleSalesPersonChange} value={salesPerson} name="sales_person" id="sales_person" className="form-select">
          <option value="">Choose a sales person</option>
          {salesPeople.map(sales_person => {
            return (
              <option key={sales_person.employee_number} value={sales_person.employee_number}>
                {sales_person.name}
              </option>
            )
          })}
        </select>
      </div>
      <table className="table table-stripped">
        <thead>
          <tr>
            <th>Sales person</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale price</th>
          </tr>
        </thead>
        <tbody>
          {saleRecords.map(sale_record => {
            return (
              <tr key={sale_record.id}>
                <td>{ sale_record.employee_number.name }</td>
                <td>{ sale_record.customer_name.name }</td>
                <td>{ sale_record.automobile.vin }</td>
                <td>${ sale_record.sale_price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesPersonHistoryList;
