import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Create a model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Create an automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_person">Sales person</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_person/new">Create a sales person</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_person/history">Sales person history</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Create a customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sale_records">Sale record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sale_records/new">Create a sale record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-item nav-link" to="/create/Employee">Add Employee</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-item nav-link" to="/create/appointment">Create an Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-item nav-link" to="/appointment/list">View Appointments</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
