import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AppointmentForm from './AppointmentForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import MainPage from './MainPage';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelForm from './ModelForm';
import ModelList from './ModelList';
import Nav from './Nav';
import EmployeeForm from './EmployeeForm';
import AppointmentList from './AppointmentList';
import SaleRecordForm from './SaleRecordForm';
import SaleRecordList from './SaleRecordList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonHistoryList from './SalesPersonHistoryList';
import SalesPersonList from './SalesPersonList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/create/appointment" element={<AppointmentForm />} />
          <Route path="/create/Employee" element={<EmployeeForm />} />
          <Route path="/appointment/list" element={<AppointmentList />} />
          <Route path="sales_person">
            <Route path="" element={<SalesPersonList />} />
            <Route path="new" element={<SalesPersonForm />} />
            <Route path="history" element={<SalesPersonHistoryList />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sale_records">
            <Route path="" element={<SaleRecordList />} />
            <Route path="new" element={<SaleRecordForm />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
