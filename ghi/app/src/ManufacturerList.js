import { useEffect, useState } from "react";


function ManufacturerList(props) {
  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturerData = async () => {
    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(manufacturersUrl);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchManufacturerData();
  }, []);

  return (
    <div className="container">
      <h2>Manufacturers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
