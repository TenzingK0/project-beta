import { useState } from "react";

function EmployeeForm() {
    const [name, setName] = useState('');
    const handlenameChange = event => {setName(event.target.value);
    }

    const [number, setNumber] =  useState ('');
    const handlenumberChange = event => {setNumber(event.target.value);
    }


    const handleSubmit = async event => {
        event.preventDefault();

        const data = {};
        data.tech_name = name;
        data.employee_number = number;
        const technicianURL = 'http://localhost:8080/api/service/employee/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
            setName('');
            setNumber('');
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create an Employee</h1>
                <form onSubmit={handleSubmit} id="create-Technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={handlenameChange} placeholder="Technician_Name" required
                            type="text" name="Technician_name" id="Technician_name" value={name}
                            className="form-control"/>
                        <label htmlFor="Technician_name">Technician Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlenumberChange} placeholder="Employee_Number" required
                            type="text" name="Employee_Number" id="Employee_Number" value={number}
                            className="form-control"/>
                        <label htmlFor="Employee_Number">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}
export default EmployeeForm
