# CarCar

Team:

* Person 1 - Sales Person - Tenzing Khantse
* Person 2 - Services - Mike Mielnicki

## Design
![CarCar Design](CarCar_Design.png)

## How to Install
    1. Go to your desired directory in your terminal
    2. Clone the repository - https://gitlab.com/TenzingK0/project-beta.git
    3. Change your directory to the newly created directory
    4. Open Docker Desktop and run the following commands in your terminal:
        docker volume create beta-data
        docker-compose build
        docer-compose up
    5. After Docker is done loading you can see view the application in your browser by going to http://localhost:3000/

## Inventory Microservice
    Models
      1. Manufacturer
        -name
      2. VehicleModel
        -name
        -picture_url
        -manufacturer (ForeignKey to Manufacturer)
      3. Automobile
        -color
        -year
        -vin
        -model (ForeignKey to VehicleModel)

  RESTful API on Port 8100

  ## Manufacturer

  ![Manufacturer path](Manufacturer_RESTful_API_path.PNG)

  Example

  Insomnia GET
  list all manufacturers

  output

    {
      "manufacturers": [
        {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      ]
    }

  show a specific manufacturer
  output

    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Chrysler"
    }

  Insomnia POST + PUT
  input

    {
    "name": "Chrysler"
    }

  output

    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Chrysler"
    }

  ## VehicleModel

  ![VehicleModel path](VehicleModel_API_path.PNG)

  Example

  Insomnia GET
  List all vehicle models
  output

    {
      "models": [
        {
          "href": "/api/models/1/",
          "id": 1,
          "name": "Sebring",
          "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
          "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "Daimler-Chrysler"
          }
        }
      ]
    }

  show a specific vehicle model
  output

    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }

  Insomnia POST
  input

    {
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer_id": 1
    }

  output

    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }

  Insomnia PUT
  input

    {
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
    }

  output

    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }

  ## Automobile

  ![Automobile path](Automobile_API_path.PNG)

  Example

  Insomnia GET
  List all automobiles
  output

    {
      "autos": [
        {
          "href": "/api/automobiles/1C3CC5FB2AN120174/",
          "id": 1,
          "color": "yellow",
          "year": 2013,
          "vin": "1C3CC5FB2AN120174",
          "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
            "manufacturer": {
              "href": "/api/manufacturers/1/",
              "id": 1,
              "name": "Daimler-Chrysler"
            }
          }
        }
      ]
    }

  list a specific automobile
  output

    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }

  Insomnia POST
  input

    {
      "color": "red",
      "year": 2012,
      "vin": "1C3CC5FB2AN120174",
      "model_id": 1
    }

  output

    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }

  Insomnia PUT
  input

    {
      "color": "red",
      "year": 2012
    }

  output

    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }

  ## React URLs

  ![Inventory React](Inventory_React_URLs.PNG)


## Service microservice
Port 3000 for React Front End;
Add an employee: http://localhost:3000/create/Employee
Add an Appointment:http://localhost:3000/create/appointment
View Appointments:http://localhost:3000/appointment/list

Port 8080 for Insomnia;
List technicians (GET): http://localhost:8080/api/service/employee/
Create a technician (POST): http://localhost:8080/api/service/employee/

List services (GET): http://localhost:8080/api/service/appointments/
Get a specific service (GET):http://localhost:8080/api/service/appointments/id
Create a service (POST): http://localhost:8080/api/service/appointments/
Update a specific service (PUT): http://localhost:8080/api/service/appointments/id/
Delete a specific service (DELETE): http://localhost:8080/api/service/appointments/id/
List all Automobile VOs to reference against the list of automobiles in invetory to verify that the poller is running: http://localhost:8100/api/automobiles/
<br>

## Creating an Technician
Using the following endpoint in insomnia will generate the following response:
Endpoint: http://localhost:8080/api/service/employee/

Following are properties required to generate desired responses:

Input:

    {
      "tech_name": "Bill",
        "employee_number": "1"
    }

output:

    {
      "technician": {
        "tech_name": "Bill",
        "employee_number": "1",
        "id": 1
      }
    }

<br>

## Creating an Appointment
Using the following endpoint in insomnia will generate the following response:
Endpoint: http://localhost:8080/api/service/appointments/

Following are properties required to generate desired responses:

input:

    {
      "vin": "1234567890123456",
      "owner_name":  "Josh",
      "service_time": "2023-03-21",
      "assigned_tech": 1,
      "service_reason":"Caroline"
    }

expected output:
    {
      "vin": "12345678901234567",
      "owner_name": "Josh",
      "service_time": "2023-03-21",
      "assigned_tech": {
        "tech_name": "Bill",
        "employee_number": "1",
        "id": 1
      },
      "service_reason": "Caroline",
      "vip": false,
      "iscomplete": false,
      "id": 1,
      "status": "Waiting"
    }

<br>

## Getting Technician List Returns:
using the following endpoint in insomnia will generate the following response:
endpoint: http://localhost:8080/api/service/employee/


expected output:

    {
      "technician": [
        {
          "tech_name": "",
          "employee_number": "",
          "id": 1
        },
      ]
    }

<br>

## Getting List of Appointments Returns:
using the following endpoint in insomnia will generate the following response:
endpoint: http://localhost:8080/api/service/appointments/

expected output:

    {
      "appointments": [
        {
          "vin": "1234567890123456",
          "owner_name": "Josh",
          "service_time": "2023-03-21T12:03:00+00:00",
          "assigned_tech": {
            "tech_name": "Bill",
            "employee_number": "1",
            "id": 1
          },
          "service_reason": "Caroline",
          "vip": false,
          "iscomplete": false,
          "id": 1,
          "status": "Waiting"
        },
      ]
    }

<br>

## PUT Request to alter VIP status:
using the following endpoint in Insomnia will generate the following response:
endpoint: http://localhost:8080/api/service/appointments/id/


Following are properties required to generate desired responses:

input:

    {
      "vin": "1234567890123456",
      "owner_name": "Josh",
      "service_time": "2023-03-16T02:02:00+00:00",
      "assigned_tech": 1,
      "iscomplete": true,
      "vip": true,
      "id": 1
    }

expected output:


    {
      "vin": "1234567890123456",
      "owner_name": "Josh",
      "service_time": "2023-03-16T02:02:00+00:00",
      "assigned_tech": 1,
      "iscomplete": true,
      "vip": true,
      "id": 1
    }

<br>

## Delete an Appointment
Using the following endpoint in Insomnia will generate the following response:
endpoint: http://localhost:8080/api/service/appointments/id/

expected output:

    {
      "deleted": true
    }

<br>


## Poller Verification
using the following endpoint will generate the following response:
Use Insomnia endpoint: http://localhost:8080/api/automobilevo/:

Output mirrors inventory at: http://localhost:8100/api/automobiles/.

Input in Inventory:

    {
      "autos": [
        {
          "href": "/api/automobiles/test/",
          "id": 1,
          "color": "red",
          "year": 2012,
          "vin": "test",
          "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
            "manufacturer": {
              "href": "/api/manufacturers/1/",
              "id": 1,
              "name": "Chrysler"
            }
          }
        },
      ]
    }

expected output:

    {
      "appointments": [
        {
          "vin": "test",
          "import_href": "/api/automobiles/test/"
        },
        {
          "vin": "testt",
          "import_href": "/api/automobiles/testt/"
        }
      ]
    }

<br>

## Services Models
Technician Model: Retains the Employee name and Employee identification number used for tracking of
employee and their past and current appointments they are assigned to.

Appointment Model: the Appointment model contains the VIN, Owner name, Assigned technician,
date time of appointment, VIP status, completion statuses, and reason for service.
There is a foreign key associated to the Technician model to correlate technician to assigned appointments.
Status is defaulted as False for VIP and completion status.

AutomobileVO: This Value Object, returns the VIN number of Vehicles found in the Inventory. If the Vehicle is found in the
inventory, the poller retrieves it. The desired outcome is to have the Valued Object data, to alter the VIP status of False to
True, Rendering them a VIP and giving them VIP service in the appointment when they arrive.


## Sales microservice
    Models
      1. AutomobileVO
        -import_href
        -vin
        -model_name
      2. SalesPerson
        -name
        -employee_number
      3. Customer
        -name
        -address
        -phone_number
      4. SaleRecord
        -sale_price
        -employee_number (ForeignKey to SalesPerson)
        -customer_name (ForeignKey to Customer)
        -automobile (ForeignKey to AutomobileVO)

  RESTful API on Port 8090

  ## Sales Person

  ![sales person api path](Sales_person_API_path.PNG)

  Examples

  Insomnia GET
  List all sales people
  output

    {
      "sales_person": [
        {
          "href": "/api/sales_person/1/",
          "name": "Patrick Star",
          "employee_number": "347658931",
          "id": 1
        },
        {
          "href": "/api/sales_person/2/",
          "name": "Spongebob Squarepants",
          "employee_number": "328764998",
          "id": 2
        }
      ]
    }

  Show a specific sales person
  output

    {
      "href": "/api/sales_person/1/",
      "name": "Brian House",
      "employee_number": "132145678"
    }

  Insomnia POST
  input

    {
      "name": "Brian House",
      "employee_number": "132145678"
    }

  output

    {
      "href": "/api/sales_person/1/",
      "name": "Brian House",
      "employee_number": "132145678"
    }

  Insomnia DELETE
  output

    {
      "deleted": true
    }

  ## Customer

  ![customer api path](Customer_API_path.PNG)

  Example
  Insomnia GET
  List all customers
  output

    {
      "customers": [
        {
          "href": "/api/customers/1/",
          "name": "Philip Wayne",
          "address": "12 Example St, Example city, CA, 19223",
          "phone_number": "215-310-1099",
          "id": 1
        }
      ]
    }

  Show a specific customer
  output

    {
      "href": "/api/customers/1/",
      "name": "Philip Wayne",
      "address": "12 Example St, Example city, CA, 19223",
      "phone_number": "215-310-1099"
    }

  Insomnia POST
  input

    {
      "name": "Philip Wayne",
      "address": "12 Example St, Example city, CA, 19223",
      "phone_number": "215-310-1099"
    }

  output

    {
      "href": "/api/customers/1",
      "name": "Philip Wayne",
      "address": "12 Example St, Example city, CA, 19223",
      "phone_number": "215-310-1099"
    }

  Insomnia DELETE
  output

    {
      "deleted": true
    }

  ## Sale Record

  ![sale record api path](Sale_records_API_path.PNG)
  example
  Insomnia GET
  List all sale records
  output

    {
      "sale_records": [
        {
          "href": "/api/sale_records/1/",
          "sale_price": 5700,
          "employee_number": {
            "href": "/api/sales_person/1/",
            "name": "Brian House",
            "employee_number": "132145678",
            "id": 1
          },
          "customer_name": {
            "href": "/api/customers/1/",
            "name": "Philip Wayne",
            "address": "12 Example St, Example city, CA, 19223",
            "phone_number": "215-310-1099",
            "id": 1
          },
          "id": 1,
          "automobile": {
            "model_name": "Sebring",
            "vin": "1C3CC5FB2AN120174"
          }
        },
        {
          "href": "/api/sale_records/3/",
          "sale_price": 5000,
          "employee_number": {
            "href": "/api/sales_person/1/",
            "name": "Brian House",
            "employee_number": "132145678",
            "id": 1
          },
          "customer_name": {
            "href": "/api/customers/1/",
            "name": "Philip Wayne",
            "address": "12 Example St, Example city, CA, 19223",
            "phone_number": "215-310-1099",
            "id": 1
          },
          "id": 3,
          "automobile": {
            "model_name": "Voyager",
            "vin": "1C9CC5FB2AN120174"
          }
        },
        {
          "href": "/api/sale_records/4/",
          "sale_price": 6000,
          "employee_number": {
            "href": "/api/sales_person/3/",
            "name": "Spongebob Squarepants",
            "employee_number": "457321890",
            "id": 3
          },
          "customer_name": {
            "href": "/api/customers/2/",
            "name": "Bose Gustav",
            "address": "32 Joy Ridge Ave, Wyandotte, MI 48192",
            "phone_number": "215-789-4321",
            "id": 2
          },
          "id": 4,
          "automobile": {
            "model_name": "Voyager",
            "vin": "1C9CC5FB2AN122378"
          }
        }
      ]
    }

  Show a specific sale record
  output

    {
      "sale_record": {
        "href": "/api/sale_records/1/",
        "sale_price": 5700,
        "employee_number": {
          "href": "/api/sales_person/1/",
          "name": "Brian House",
          "employee_number": "132145678",
          "id": 1
        },
        "customer_name": {
          "href": "/api/customers/1/",
          "name": "Philip Wayne",
          "address": "12 Example St, Example city, CA, 19223",
          "phone_number": "215-310-1099",
          "id": 1
        },
        "id": 1,
        "automobile": {
          "model_name": "Sebring",
          "vin": "1C3CC5FB2AN120174"
        }
      }
    }

  Insomnia POST
  input

    {
      "sale_price": 5700,
      "employee_number": 1,
      "customer_name": 1,
      "automobile": "/api/automobiles/1C3CC5FB2AN120174/"
    }

  output

    {
      "sale_record": {
        "href": "/api/sale_records/1/",
        "sale_price": 5700,
        "employee_number": {
          "href": "/api/sales_person/1/",
          "name": "Brian House",
          "employee_number": "132145678",
          "id": 1
        },
        "customer_name": {
          "href": "/api/customers/1/",
          "name": "Philip Wayne",
          "address": "12 Example St, Example city, CA, 19223",
          "phone_number": "215-310-1099",
          "id": 1
        },
        "id": 1,
        "automobile": {
          "model_name": "Sebring",
          "vin": "1C3CC5FB2AN120174"
        }
      }
    }

  Insomnia DELETE
  output

    {
      "deleted": true
    }

  ## React URls

  ![Sales React](Sales_React_URLs.PNG)
