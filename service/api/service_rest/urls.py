from .views import api_service_history, api_list_appointment, api_create_technician, poller_test
from django.urls import path


urlpatterns = [
    path('service/appointments/', api_list_appointment, name='api_list_appointments'),
    path('service/appointments/<int:id>/', api_service_history, name= 'api_service_history'),
    path('service/employee/', api_create_technician, name='api_create_technician'),
    path('automobilevo/', poller_test, name= 'poller_test'),
]
