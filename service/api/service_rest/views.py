from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Appointment, Technician, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[
        'vin',
        'import_href',
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties =[
        'tech_name',
        'employee_number',
        'id',
    ]
class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties =[
        'vin',
        'owner_name',
        'service_time',
        'assigned_tech',
        'service_reason',
        'vip',
        'iscomplete',
        'id',
        'status',
    ]
    encoders={
        'assigned_tech': TechnicianEncoder(),
    }

# Create your views here.
#show a list of scheduled appointments that contain the details collected in the form:
#VIN, Customer name, date and time of apppointment, the assigned techs name, and the reason for service.
#if VIN was in inventory, then the automobile was purchased from the dealership. Must give "VIP" Treatment
#Delete when cancelled,

#show a list of the service appointments for a specific VIN. Includes customer name, date and time of appointment, assigned
#techs name, and the reason for service.


def api_create_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianEncoder,
            safe=False
        )
    elif request.method == "POST":
        content= json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianEncoder,
            safe=False,
        )


def api_list_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    elif request.method == "POST":
        content= json.loads(request.body)
        try:
            assigned_tech = Technician.objects.get(id=content["assigned_tech"])
            content["assigned_tech"]=assigned_tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Technican Id"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT","DELETE"])
def api_service_history(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder = AppointmentListEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ =Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})
    else:
        content = json.loads(request.body)
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.finish_appointment()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "invalid Appointment id"},
                status=400,
            )

        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )

def poller_test(request):
    if request.method == "GET":
        appointments = AutomobileVO.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AutomobileVOEncoder,
            safe=False
        )
