from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, default=None)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

class Technician(models.Model):
    tech_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    owner_name = models.CharField(max_length=100)
    service_time = models.DateTimeField()
    assigned_tech = models.ForeignKey(
        Technician,
        related_name = "appointment",
    on_delete=models.CASCADE,
)
    service_reason = models.TextField(max_length = 250)
    vip = models.BooleanField(default=False)
    iscomplete = models.BooleanField(default= False)
    status = models.CharField(max_length= 15, default ='Waiting')

    def __str__(self):
        return self.name

    def finish_appointment(self):
        self.iscomplete = True
        self.save()
